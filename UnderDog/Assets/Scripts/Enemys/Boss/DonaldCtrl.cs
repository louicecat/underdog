using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DonaldCtrl : MonoBehaviour
{
    public Text bossHeal;

    [Header("����I��")]
    public GameObject donaldCollider;
    [Header("���D�y��")]
    public GameObject jumpTarget;
    public GameObject m_Camera;
    
    Transform playerTarget;//�p��Z��
    Transform attackTarget;
    public float playerRange_x;
    public float playerRange_y;
    public float playerRange;

    static public float hpPhase = 1;//�ĴX���q
    [Header("�C���q��q")]
    static public float hp = 20;//�@���q20��

    [Header("�^��t��")]
    public float hpAddTimeSet = 1;
    float hpAddCount = 0;//�ɦ�p��
                         
    [Header("�i�^��ɶ�")]
    public float hpCanAddTimeSet = 2;
    float hpCanAddCount = 0;//�Q���ɦ夤�_

    //�P�_�����ɥ��k
    bool isLeft;
    bool isRight;

    //0�S�����A1���Y�L�B1.1���Y�L���ݧP�w�A2���D�����A3���b���Y�A4�Y���E���A5��h�u�A6����ļ��A7���D����3�s
    public float attackType = 0;
    [Header("���Y�L")]
    public GameObject readyAttack;
    //���Y�L������
    public GameObject m_Attack;
    //���Y�L�N�o
    float attackColdSet = 2;
    float attackColdCount;
    float attackCount = 0;
    float s_Miss = 1;


    [Header("�t��")]
    public float speed = 0.5f;

    [Header("���D����")]
    public GameObject m_Attack_1;
    float jumpRange_x, jumpRange_y, jumpRange;

    [Header("���b����")]
    public GameObject m_Attack_2;

    void Start()
    {
        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        hpPhase = 1;
        hp = 20;
    }

    void Update()
    {
        HpStage();
        HpAdd();
        if (s_Miss <= 0)
        {
            Attack();
            Move();
        }
        else
        {
            s_Miss -= Time.deltaTime;
        }
    }

    /*void OnDrawGizmos()//���U�u
    {
        Gizmos.DrawWireCube(gameObject.transform.position, new Vector3(3.6f, 3, 0));
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(gameObject.transform.position , 3f);
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Attack")//�Q����
        {
            hp -= 1;
            hpCanAddCount = 0;
            hpAddCount = 0;
            if (playerTarget.position.x > transform.position.x && transform.eulerAngles.y == 180)
            {
                hp -= 1;
            }
            if (playerTarget.position.x < transform.position.x && transform.eulerAngles.y == 0)
            {
                hp -= 1;
            }
        }
    }

    void HpStage()//�������q
    {
        if (hp <= 0 && hpPhase == 1)
        {
            hpPhase = 2;
            LevelSetting.deadTimeCount += 20;
        }

        if (hp <= 0 && hpPhase == 2)
        {
            LevelSetting.deadTimeCount += 20;
            Destroy(gameObject);
        }
    }

    void HpAdd()//�ɦ�
    {
        if (hp < 20)
        {
            if (hpCanAddCount <= hpCanAddTimeSet)
            {
                hpCanAddCount += Time.deltaTime;
            }
            else
            {
                hpAddCount += Time.deltaTime;
                if (hpAddCount >= hpAddTimeSet)
                {
                    hp += 1;
                    hpAddCount = 0;
                }
            }
        }
    }

    void Move()
    {
        if (attackType == 0)
        {

            if (playerTarget.transform.position.x > transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
            }
            if (playerTarget.transform.position.x < transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
            }

            if (playerRange > 11 ||
                3 <= playerRange_x && playerRange_x <= 3.5f ||
                3 >= playerRange_x && playerRange_y > 0.2f) 

            {
                transform.position = Vector2.MoveTowards(transform.position, playerTarget.position, -speed * Time.deltaTime * 3);
            }
        }
    }

    void Attack()
    {
        //�Z��
        playerRange_x = Mathf.Abs(playerTarget.position.x - gameObject.transform.position.x);
        playerRange_y = Mathf.Abs(playerTarget.position.y - gameObject.transform.position.y);
        playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

        if(attackType == 0)
        {
            m_Attack.SetActive(false);
            m_Attack_1.SetActive(false);
        }
        //�P�_��������
        //0�S�����A1���Y�L�A2���D�����A3���b���Y�A4�Y���E���A5��h�u�A6����ļ��A7���D����3�s
        if (playerRange_x <= 3 && playerRange_y <= 0.2 && attackType == 0 && attackColdCount <= 0 && hpPhase == 1)
        {
            attackType = 1;
        }
        if (playerRange >= 3.5 && playerRange <= 5.5 && attackType == 0 && attackColdCount >= 0 && hpPhase == 1)
        {
            attackType = 2;
        }
        if (playerRange > 5.5 && playerRange <= 11 && attackType == 0 && m_Attack_2.activeSelf == false && hpPhase == 1)
        {
            DonaldRocketHand.gotPlayer = false;
            attackType = 3;
            m_Attack_2.SetActive(true);
        }
        if (playerRange_x < 0.05f && attackType == 0 && hpPhase == 2)
        {
            attackType = 4;
        }

        //���Y�L
        if (attackType == 1)//���Y�L�w��
        {

            if (playerTarget.position.x - gameObject.transform.position.x > 0)
            {
                isRight = true;
            }
            if (playerTarget.position.x - gameObject.transform.position.x <= 0)
            {
                isLeft = true;
            }
            attackCount = 0;
            attackType = 1.1f;
        }
        else if (attackType == 1.1f)//���Y�L�wĵ
        {
            readyAttack.SetActive(true);
            attackCount += Time.deltaTime;

            if (attackCount >= 1f)
            {
                readyAttack.SetActive(false);
                m_Attack.SetActive(true);

                attackCount = 0;
                attackType = 1.2f;
            }
        }
        else if (attackType == 1.2f)//���Y�L�ĩ�
        {
            Debug.Log("���Y�L�ĩ�");
            attackCount += Time.deltaTime;

            if (isRight == true)
            {
                transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 4;
            }
            if (isLeft == true)
            {
                transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 4;
            }

            if (attackCount >= 1)
            {
                m_Attack.SetActive(false);
                isRight = false;
                isLeft = false;
                attackType = 0;
                attackCount = 0;
                s_Miss = 1;
                attackColdCount = attackColdSet - s_Miss;
            }

            if (HugHeadTrigger.hugHeadTriggerGet == true)
            {
                attackType = 1.3f;
                attackCount = 0;
            }
        }
        else if (attackType == 1.3f)//���Y�L�ߺL
        {
            Debug.Log("���Y�L�ߺL");
            attackCount += Time.deltaTime;

            if (isRight == true)
            {
                transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 4;
            }
            if (isLeft == true)
            {
                transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 4;
            }
            if (attackCount > 1)
            {
                attackCount = 0;
                attackType = 1.4f;
            }
        }
        else if (attackType == 1.4f)//���Y�L��M
        {
            Debug.Log("���Y�L��M");
            attackCount += Time.deltaTime;
            if (isRight == true)
            {
                transform.position = transform.position - Vector3.right * Time.deltaTime * speed * 6;
            }
            if (isLeft == true)
            {
                transform.position = transform.position - Vector3.left * Time.deltaTime * speed * 6;
            }
            if (attackCount > 1)
            {
                HugHeadTrigger.hugHeadTriggerGet = false;
                isRight = false;
                isLeft = false;
                attackCount = 0;
                attackColdCount = attackColdSet;
                attackType = 0;
            }
        }
        //���Y�L�N�o
        if (attackColdCount > 0)
        {
            attackColdCount -= Time.deltaTime;
        }

        //���D����
        if (attackType == 2)//��w
        {
            Instantiate(jumpTarget, playerTarget.transform.position, transform.rotation);
            attackType = 2.1f;
        }
        if(attackType == 2.1f)//���D
        {
            if (attackTarget == null)
            {
                attackTarget = GameObject.FindGameObjectWithTag("JumpTarget").GetComponent<Transform>();
                jumpRange_x = Mathf.Abs(attackTarget.position.x - gameObject.transform.position.x);
                jumpRange_y = Mathf.Abs(attackTarget.position.y - gameObject.transform.position.y);
                jumpRange = Mathf.Sqrt(jumpRange_x * jumpRange_x + jumpRange_y + jumpRange_y);
            }
            if (attackTarget != null)
            {
                transform.position = Vector2.MoveTowards(transform.position, attackTarget.position, speed * jumpRange * 2 * Time.deltaTime);
            }
            attackCount += Time.deltaTime;
            donaldCollider.SetActive(false);
            if (attackCount > 1)
            {
                attackType = 2.2f;
                attackCount = 0;
            }
        }
        if(attackType == 2.2f)//����
        {
            attackCount += Time.deltaTime;
            donaldCollider.SetActive(true);
            m_Attack_1.SetActive(true);
            if (attackCount > 0.1f)
            {
                attackType = 2.3f;
                attackCount = 0;
            }
        }
        if(attackType == 2.3f)//�w��
        {
            attackCount += Time.deltaTime;
            m_Attack_1.SetActive(false);
            if(attackCount > 2)
            {
                attackType = 0;
                attackCount = 0;
            }
        }

        //���b����
        if(attackType == 3)
        {
            if (DonaldRocketHand.gotPlayer == true)
            {
                attackType = 3.1f;
            }
            else if(m_Attack_2.activeSelf == false)
            {
                attackType = 0;
            }
        }

        if (attackType == 3.1f)//�w��
        {
            attackCount += Time.deltaTime;
            m_Attack_1.SetActive(false);
            if (attackCount > 1)
            {
                attackType = 1f;
                attackCount = 0;
            }
        }
    }
}
