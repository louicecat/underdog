using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarCtrl : MonoBehaviour
{
    Rigidbody2D rb;
    public bool beAlarmed;
    public float playerRange_x;
    public float playerRange_y;
    float playerRange = 30;
    Transform playerTarget;

    float starkTimeCount;

    float attackStep;

    [Header("速度")]
    public float speed = 0.75f;
    [Header("HP")]
    public int Hp = 3;
    [Header("受擊硬直")]
    public float s_TimeSet = 0.5f;

    public float attackTimeCount = 0;
    [Header("預警")]
    public float attackReadyTime = 0.5f;
    [Header("攻擊")]
    public float attackTime = 0.1f;
    [Header("硬直")]
    public float attackAfterTime = 1.4f;

    public GameObject attack;
    public GameObject readyAttack;

    bool attacking;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        playerRange_x = Mathf.Abs(playerTarget.position.x - gameObject.transform.position.x);
        playerRange_y = Mathf.Abs(playerTarget.position.y - gameObject.transform.position.y);
        playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

        if (beAlarmed == false)
        {
            if (playerRange < 9)
            {
                beAlarmed = true;
            }
        }
        if (beAlarmed == true)
        {
            if (starkTimeCount <= 0)
            {
                rb.velocity = new Vector3(0, 0, 0);
                Attack();
                if (attackStep == 0)
                    Move();
            }
            else
            {
                starkTimeCount -= Time.deltaTime;
            }
        }

    }

    //void OnDrawGizmos()//輔助線
    //{
    //    Gizmos.DrawWireCube(gameObject.transform.position, new Vector3(7f, 0.5f, 0));
    //    Gizmos.color = Color.red;
    //    //Gizmos.DrawWireSphere(gameObject.transform.position, 3f);
    //}

    void Attack()
    {
        //距離
        playerRange_x = Mathf.Abs(playerTarget.position.x - gameObject.transform.position.x);
        playerRange_y = Mathf.Abs(playerTarget.position.y - gameObject.transform.position.y);
        playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

        if (playerRange_x < 5.6f && playerRange_y < 0.3f && attackStep == 0)
        {
            if (playerTarget.transform.position.x > transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
            }
            if (playerTarget.transform.position.x < transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
            }
            attackStep = 1;
        }

        if (attackStep == 1)
        {
            readyAttack.SetActive(true);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackReadyTime)
            {
                attackTimeCount = 0;
                attackStep = 2;
            }
        }
        if (attackStep == 2)
        {
            readyAttack.SetActive(false);
            attack.SetActive(true);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackTime)
            {
                attackTimeCount = 0;
                attackStep = 3;
            }
        }
        if (attackStep == 3)
        {
            attack.SetActive(false);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackAfterTime)
            {
                attackTimeCount = 0;
                attackStep = 0;
            }
        }
    }

    void Move()
    {
        if (playerTarget.transform.position.x > transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (playerTarget.transform.position.x < transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        if (playerTarget.transform.position.y - transform.position.y > .28f
            && playerTarget.transform.position.x - transform.position.x <= 5.6f
            && playerTarget.transform.position.x - transform.position.x >= -5.6f)
        {
            gameObject.transform.position += Vector3.up * Time.deltaTime * speed * 6;
        }
        if (playerTarget.transform.position.y - transform.position.y < .28f
            && playerTarget.transform.position.x - transform.position.x <= 5.6f
            && playerTarget.transform.position.x - transform.position.x >= -5.6f)
        {
            gameObject.transform.position += Vector3.down * Time.deltaTime * speed * 6;
        }
        if (playerTarget.transform.position.x - transform.position.x > -5.6f)
        {
            gameObject.transform.position += Vector3.right * Time.deltaTime * speed * 6;
        }
        if (playerTarget.transform.position.x - transform.position.x < 5.6f)
        {
            gameObject.transform.position += Vector3.left * Time.deltaTime * speed * 6;
        }   
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Attack")
        {
            starkTimeCount = s_TimeSet;

            //補玩家血
            LevelSetting.deadTimeCount += 1;
            //扣血
            Hp -= 1;
            //死亡
            if (Hp <= 0)
            {
                Destroy(gameObject);//到時候改動畫
            }

            if (playerTarget.gameObject.transform.position.x > transform.position.x)
                rb.velocity = new Vector3(-1.041f, 0, 0);
            if (playerTarget.gameObject.transform.position.x < transform.position.x)
                rb.velocity = new Vector3(1.041f, 0, 0);
        }
    }
}
