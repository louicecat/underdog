using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageClearCtrl : MonoBehaviour
{
    float timeCount;
    [Header("�X�{�ɶ�")]
    public float timeSet = 1;

    // Update is called once per frame
    void Update()
    {
        timeCount += Time.deltaTime;
        if(timeCount > timeSet)
        {
            gameObject.SetActive(false);
            timeCount = 0;
        }
    }
}
