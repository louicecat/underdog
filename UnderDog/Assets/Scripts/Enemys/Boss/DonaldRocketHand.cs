using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonaldRocketHand : MonoBehaviour
{
    GameObject m_Boss;
    GameObject m_Player;
    public GameObject m_RocketHandPack;
    [Header("基礎速度")]
    public float speed = 0.5f;
    [Header("起點")]
    public GameObject m_Start;
    [Header("終點")]
    public GameObject m_End;

    float attackStep = 1;

    public float attackTimeCount;

    public static bool gotPlayer = false;

    private void Start()
    {
        m_Boss = GameObject.FindGameObjectWithTag("Donald");
        m_Player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (attackStep == 1)
        {
            Vector2 direction = m_Player.transform.position - m_Boss.transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            m_RocketHandPack.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            attackStep = 2;
        }
        if (attackStep == 2)
        {
            gameObject.transform.position += gameObject.transform.right * Time.deltaTime * speed * 12;
        }
        if (attackStep == 3)
        {
            attackTimeCount += Time.deltaTime;
            if(attackTimeCount >= 2)
            {
                attackTimeCount = 0;
                attackStep = 4;
            }
        }
        if (attackStep == 4)
        {
            gameObject.transform.position -= gameObject.transform.right * Time.deltaTime * speed * 12;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            attackStep = 4;
            gotPlayer = true;
        }
        if (collision.tag == "RocketHandEnd")
        {
            attackStep = 3;
        }
        if (collision.tag == "RocketHandStart" && attackStep == 4)
        {
            attackStep = 1;
            gameObject.SetActive(false);
        }
    }
}
