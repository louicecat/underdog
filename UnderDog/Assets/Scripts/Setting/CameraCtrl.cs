using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrl : MonoBehaviour
{
    GameObject m_Player;
    public Vector2 minPosition;
    public Vector2 maxPosition;

    private void Update()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player");

        if (LevelSetting.nowLevel == 0)
            transform.position = new Vector3(0, 0, -10);
        if (LevelSetting.nowLevel != 0)
        {
            Vector3 playerPos = m_Player.transform.position;
            playerPos.x = Mathf.Clamp(playerPos.x, minPosition.x, maxPosition.x);
            transform.position = new Vector3(playerPos.x, 0, -10);
        }
    }
}
