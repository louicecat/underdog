using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HugHeadTrigger : MonoBehaviour
{
    public static bool hugHeadTriggerGet;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag =="Player")
        {
            hugHeadTriggerGet = true;
            gameObject.SetActive(false);
        }
    }
}
