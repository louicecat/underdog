using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCtrl : MonoBehaviour
{
    [Header("主角動畫控制器")]
    public Animator a_Player;
    [Header("戰鬥主角動畫控制器")]
    public Animator a_FightPlayer;
    Rigidbody2D rb;

    [Header("主角控制器")]
    public GameObject playerController;
    //[Header("麻痺")]
    //public GameObject m_Paralysis;
    //[Header("中毒")]
    //public GameObject m_Poisoning;

    Transform donaldTarget;//定位唐納德

    //攻擊
    float attackTimeCount = 0;
    float attackLinkTimeCount = 0;
    [Header("攻擊冷卻時間")]
    public float attackTimeSet = 0.33f;
    [Header("攻擊銜接時間")]
    public float attackLinkTimeSet = 0.2f;
    [Header("普通攻擊第一、二段")]
    public GameObject m_attack;
    [Header("普通攻擊第三段")]
    public GameObject m_attack_1;
    public bool b_IsAttack = false;
    bool b_AttackLink_1 = false;
    bool b_AttackLink_2 = false;
    bool b_AttackReset;
    //移動
    [Header("移動速度")]
    public float speed;
    bool cantUp;
    bool cantDown;
    bool cantLeft;
    bool cantRight;
    bool b_FinishPoint;
    bool b_BackPoint;

    //float rushTimeCount = 0;
    //[Header("衝刺冷卻時間")]
    //public float rushTimeSet = 1;
    //bool isRush = false;

    [Header("跑步點擊間隔")]
    bool readyRun = false;
    //public float readyRunTimeSet = 0.2f;
    //bool isRun_U = false;
    //bool readyRun_U = false;
    //float readyRunTimeCount_U = 0;
    //bool isRun_D = false;
    //bool readyRun_D = false;
    //float readyRunTimeCount_D = 0;
    //bool isRun_L = false;
    //bool readyRun_L = false;
    //float readyRunTimeCount_L = 0;
    //bool isRun_R = false;
    //bool readyRun_R = false;
    //float readyRunTimeCount_R = 0;

    //硬直
    public static float starkTimeCount = 0;
    [Header("攻擊結束硬直時間")]
    public float s_AttackTimeSet = 0.1f;
    [Header("被攻擊硬直時間")]
    public float s_AttackedTimeSet = 0.5f;

    //互動
    public GameObject m_InteractiveButtom;
    //public GameObject m_Camera;
    public GameObject m_Player;

    //無敵幀
    public static float invincibleTimeCount;
    [Header("衝刺無敵時間")]
    public float rushInvincibleTimeSet = 0.25f;
    [Header("被攻擊無敵時間")]
    public float attackedInvincibleTimeSet = 0.5f;

    [Header("(要和唐納德<抱頭摔衝抱時間>一樣)")]
    public float hugHeadInAnimation = 1;

    //特殊狀態時間
    /*[Header("中毒持續時間")]
    public int poisoningTimeSet = 3;
    float poisoningTime;
    float poisoningTimeCount;*/

    //[Header("麻痺持續時間")]
    //public int paralysisTimeSet = 1;
    //float paralysTime;
    //float paralysisTimeCount;




    // Start is called before the first frame update
    void Start()
    {
        attackTimeCount = 0;
        b_IsAttack = false;
        m_InteractiveButtom.SetActive(false);

        rb = playerController.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        donaldTarget = GameObject.FindGameObjectWithTag("Donald").GetComponent<Transform>();

        PlayerInteractive();
        //無敵幀
        if (invincibleTimeCount >= 0)
        {
            invincibleTimeCount -= Time.deltaTime;
        }

        //硬直
        if (starkTimeCount <= 0 && PlayerHealth.paralysTime <= 0)
        {
            rb.velocity = new Vector3(0, 0, 0);
            //m_Paralysis.SetActive(false);
            if (LevelSetting.isTransitions == true)
            {
                Attack();
                if (attackLinkTimeCount <= 0)
                {
                    PlayerMove();
                }
            }
        }
        if (starkTimeCount >= 0)
        {
            starkTimeCount -= Time.deltaTime;
        }
    }

    //攻擊
    public void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && b_IsAttack == false && LevelSetting.nowLevel > 0)
        {
            if (playerController.transform.eulerAngles.y == 180)
            {
                rb.velocity = new Vector3(-2.55f, 0, 0);
            }
            if (playerController.transform.eulerAngles.y == 0)
            {
                rb.velocity = new Vector3(2.55f, 0, 0);
            }

 
            /*if (b_AttackLink_2 == true)
            {
                b_AttackLink_2 = false;
                b_AttackReset = true;
                attackLinkTimeCount = attackLinkTimeSet + attackTimeSet;
                m_attack_1.SetActive(true);
                starkTimeCount = s_AttackTimeSet;
            }*/
            if (b_AttackLink_1 == true)
            {
                b_AttackLink_1 = false;
                //b_AttackLink_2 = true;
                starkTimeCount = s_AttackTimeSet;
                attackLinkTimeCount = attackLinkTimeSet + attackTimeSet;
                m_attack_1.SetActive(true);
            }
            else if (b_AttackLink_1 == false && b_AttackReset == false)// && b_AttackLink_2 == false 
            {
                starkTimeCount = s_AttackTimeSet;
                b_AttackLink_1 = true;
                attackLinkTimeCount = attackLinkTimeSet + attackTimeSet;

                m_attack.SetActive(true);
            }

            b_AttackReset = false;
            b_IsAttack = true;
        }

        if (b_IsAttack == true)
        {
            attackTimeCount = attackTimeCount + Time.deltaTime;
            if (attackTimeCount > attackTimeSet)
            {
                b_IsAttack = false;
                attackTimeCount = 0;
            }
        }

        if(attackLinkTimeCount > 0)
        {
            attackLinkTimeCount -= Time.deltaTime;
        }
        if(attackLinkTimeCount <= 0)
        {
            b_AttackLink_1 = false;
            b_AttackLink_2 = false;
        }
    }
    //移動
    public void PlayerMove()
    {
        //衝刺冷卻
        //if (isRush == true)
        //{
        //    rushTimeCount = rushTimeCount + Time.deltaTime;
        //    if (rushTimeCount > rushTimeSet)
        //    {
        //        isRush = false;
        //        rushTimeCount = 0;
        //    }
        //}

        //跑步間隔
        //if (readyRun_U == true)
        //{
        //    readyRunTimeCount_U = readyRunTimeCount_U + Time.deltaTime;
        //    if (readyRunTimeCount_U > readyRunTimeSet)
        //    {
        //        readyRun_U = false;
        //        readyRunTimeCount_U = 0;
        //    }
        //}
        //if (readyRun_D == true)
        //{
        //    readyRunTimeCount_D = readyRunTimeCount_D + Time.deltaTime;
        //    if (readyRunTimeCount_D > readyRunTimeSet)
        //    {
        //        readyRun_D = false;
        //        readyRunTimeCount_D = 0;
        //    }
        //}
        //if (readyRun_L == true)
        //{
        //    readyRunTimeCount_L = readyRunTimeCount_L + Time.deltaTime;
        //    if (readyRunTimeCount_L > readyRunTimeSet)
        //    {
        //        readyRun_L = false;
        //        readyRunTimeCount_L = 0;
        //    }
        //}
        //if (readyRun_R == true)
        //{
        //    readyRunTimeCount_R = readyRunTimeCount_R + Time.deltaTime;
        //    if (readyRunTimeCount_R > readyRunTimeSet)
        //    {
        //        readyRun_R = false;
        //        readyRunTimeCount_R = 0;
        //    }
        //}

        //走與跑
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            readyRun = true;
        }
        if (!Input.GetKey(KeyCode.LeftShift))
        {
            a_Player.SetBool("Run", false);
            a_FightPlayer.SetBool("Run", false);
            readyRun = false;
        }
        

        //if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
        //{
        //    if (readyRun_U == true)
        //        isRun_U = true;
        //    if (readyRun_D == true)
        //        isRun_D = true;
        //    if (readyRun_L == true)
        //        isRun_L = true;
        //    if (readyRun_R == true)
        //        isRun_R = true;
        //}
        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D) && cantUp == false && cantRight == false)
        {
            if (readyRun == false)
            {
                playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                a_Player.SetBool("Walk", true);
                a_FightPlayer.SetBool("Walk", true);
                playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 2.12f;
                playerController.transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 2.12f;
            }
            if (readyRun == true)
            {
                playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                a_Player.SetBool("Walk", true);
                a_Player.SetBool("Run", true);
                a_FightPlayer.SetBool("Walk", true);
                a_FightPlayer.SetBool("Run", true);
                playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 4.24f;
                playerController.transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 4.24f;
            }
        }
        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D) && cantDown == false && cantRight == false)
        {
            if (readyRun == false)
            {
                playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                a_Player.SetBool("Walk", true);
                a_FightPlayer.SetBool("Walk", true);
                playerController.transform.position = transform.position + Vector3.down * Time.deltaTime * speed * 2.12f;
                playerController.transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 2.12f;
            }
            if (readyRun == true)
            {
                playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                a_Player.SetBool("Walk", true);
                a_Player.SetBool("Run", true);
                a_FightPlayer.SetBool("Walk", true);
                a_FightPlayer.SetBool("Run", true);
                playerController.transform.position = transform.position + Vector3.down * Time.deltaTime * speed * 4.24f;
                playerController.transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 4.24f;
            }
        }
        else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A) && cantDown == false && cantLeft == false)
        {
            if (readyRun == false)
            {
                playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                a_Player.SetBool("Walk", true);
                a_FightPlayer.SetBool("Walk", true);
                playerController.transform.position = transform.position + Vector3.down * Time.deltaTime * speed * 2.12f;
                playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 2.12f;
            }
            if (readyRun == true)
            {
                playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                a_Player.SetBool("Walk", true);
                a_Player.SetBool("Run", true);
                a_FightPlayer.SetBool("Walk", true);
                a_FightPlayer.SetBool("Run", true);
                playerController.transform.position = transform.position + Vector3.down * Time.deltaTime * speed * 4.24f;
                playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 4.24f;
            }
        }
        else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A) && cantUp == false && cantLeft == false)
        {
            if (readyRun == false)
            {
                playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                a_Player.SetBool("Walk", true);
                a_FightPlayer.SetBool("Walk", true);
                playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 2.12f;
                playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 2.12f;
            }
            if (readyRun == true)
            {
                playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                a_Player.SetBool("Walk", true);
                a_Player.SetBool("Run", true);
                a_FightPlayer.SetBool("Walk", true);
                a_FightPlayer.SetBool("Run", true);
                playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 4.24f;
                playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 4.24f;
            }
        }
        if (!(Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A)) && 
            !(Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A)) &&
            !(Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D)) &&
            !(Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D)))
        {
            if (Input.GetKey(KeyCode.A) && cantLeft == false)
            {
                if (readyRun == false)
                {
                    playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                    a_Player.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Walk", true);
                    playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 3;
                    //readyRun_L = true;
                }
                else
                {
                    playerController.transform.eulerAngles = new Vector3(0, 180, 0);
                    a_Player.SetBool("Walk", true);
                    a_Player.SetBool("Run", true);
                    a_FightPlayer.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Run", true);
                    playerController.transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 6;
                }
            }
            else if (Input.GetKey(KeyCode.D) && cantRight == false)
            {
                if (readyRun == false)
                {
                    playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                    a_Player.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Walk", true);
                    playerController.transform.position = transform.position - Vector3.left * Time.deltaTime * speed * 3;
                    //readyRun_R = true;
                }
                if (readyRun == true)
                {
                    playerController.transform.eulerAngles = new Vector3(0, 0, 0);
                    a_Player.SetBool("Walk", true);
                    a_Player.SetBool("Run", true);
                    a_FightPlayer.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Run", true);
                    playerController.transform.position = transform.position - Vector3.left * Time.deltaTime * speed * 6;
                }
            }
            else if (Input.GetKey(KeyCode.W) && cantUp == false)
            {
                if (readyRun == false)
                {
                    a_Player.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Walk", true);
                    playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 3;
                    //readyRun_U = true;
                }
                if (readyRun == true)
                {
                    a_Player.SetBool("Walk", true);
                    a_Player.SetBool("Run", true);
                    a_FightPlayer.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Run", true);
                    playerController.transform.position = transform.position + Vector3.up * Time.deltaTime * speed * 6;
                }
            }
            else if (Input.GetKey(KeyCode.S) && cantDown == false)
            {
                if (readyRun == false)
                {
                    a_Player.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Walk", true);
                    playerController.transform.position = transform.position - Vector3.up * Time.deltaTime * speed * 3;
                    //readyRun_D = true;
                }
                if (readyRun == true)
                {
                    a_Player.SetBool("Walk", true);
                    a_Player.SetBool("Run", true);
                    a_FightPlayer.SetBool("Walk", true);
                    a_FightPlayer.SetBool("Run", true);
                    playerController.transform.position = transform.position - Vector3.up * Time.deltaTime * speed * 6;
                }
            }
            else
            {
                a_Player.SetBool("Walk", false);
                a_FightPlayer.SetBool("Walk", false);
                //    if (isRun_U == true || isRun_D == true || isRun_L == true || isRun_R == true)
                //    {
                //        readyRunTimeCount_U = readyRunTimeSet;
                //        readyRunTimeCount_D = readyRunTimeSet;
                //        readyRunTimeCount_L = readyRunTimeSet;
                //        readyRunTimeCount_R = readyRunTimeSet;
                //        isRun_U = false;
                //        isRun_D = false;
                //        isRun_L = false;
                //        isRun_R = false;
                //    }
            }
        }
        //if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        

        //衝刺
        if (Input.GetKeyDown(KeyCode.Space) && LevelSetting.nowLevel > 0)// && isRush == false
        {
            invincibleTimeCount = rushInvincibleTimeSet;
            starkTimeCount = 0.25f;
            if (Input.GetKey(KeyCode.W) && cantUp == false)
            {
                rb.velocity = new Vector3(0, 9.37f, 0);
                //rb.MovePosition(transform.position + Vector3.up * speed * 3);
                //transform.position = transform.position + Vector3.up * speed * 3;
                LevelSetting.deadTimeCount = LevelSetting.deadTimeCount - 1;
                //isRush = true;
            }
            else if (Input.GetKey(KeyCode.S) && cantDown == false)
            {
                rb.velocity = new Vector3(0, -9.37f, 0);
                //rb.MovePosition(transform.position + Vector3.down * speed * 3);
                //transform.position = transform.position - Vector3.up * speed * 3;
                LevelSetting.deadTimeCount = LevelSetting.deadTimeCount - 1;
                //isRush = true;
            }
            else if (Input.GetKey(KeyCode.A) && cantLeft == false || gameObject.transform.eulerAngles.y == 180 && cantRight == false)
            {
                rb.velocity = new Vector3(-9.37f, 0, 0);
                //rb.MovePosition(transform.position + Vector3.left * speed * 3);
                //transform.position = transform.position + Vector3.left * speed * 3;
                LevelSetting.deadTimeCount = LevelSetting.deadTimeCount - 1;
                //isRush = true;
            }
            else if (Input.GetKey(KeyCode.D) && cantRight == false || gameObject.transform.eulerAngles.y == 0 && cantRight == false)
            {
                rb.velocity = new Vector3(9.37f, 0,0);
                //transform.position = transform.position - Vector3.left * speed * 3;
                LevelSetting.deadTimeCount = LevelSetting.deadTimeCount - 1;
                //isRush = true;
            }
        }
    }
    //交互
    void PlayerInteractive()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (b_FinishPoint == true)
            {
                //往第二關
            }
            if(b_BackPoint == true)
            {
                if (LevelSetting.nowLevel > 0)
                {
                    //戰鬥關卡
                    LevelSetting.nowLevel = LevelSetting.nowLevel - 1;
                    if (LevelSetting.nowLevel <= 0)
                    {
                        LevelSetting.b_Phone = true;
                        LevelSetting.b_PhoneScreen = false;
                    }
                }
                    //非戰鬥關卡
                if (LevelSetting.nowLevel < 0)
                {
                    LevelSetting.nowLevel = LevelSetting.nowLevel + 1;
                    if (LevelSetting.nowLevel <= 0)
                    {
                        LevelSetting.b_Phone = true;
                        LevelSetting.b_PhoneScreen = false;
                    }
                }
                //根據關卡傳送
                if (LevelSetting.nowLevel == 0)
                {
                    LevelSetting.nowLevel = 0;
                    m_Player.transform.position = new Vector3(-2000, 0, 0);
                    LevelSetting.isTransitions = false;
                    SceneManager.LoadScene(2);
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //受傷
        if (invincibleTimeCount <= 0)
        {
            PlayerHealth.paralysTime = 0;
            //炸彈小混混
            if (collision.gameObject.tag == "Hurt2")
            {
                invincibleTimeCount = attackedInvincibleTimeSet;
                starkTimeCount = s_AttackedTimeSet;
                LevelSetting.deadTimeCount -= 5;

                if (collision.gameObject.transform.position.x >= transform.position.x)
                    rb.velocity = new Vector3(-2.082f, 0, 0);
                if (collision.gameObject.transform.position.x < transform.position.x)
                    rb.velocity = new Vector3(2.082f, 0, 0);
            }
            //狂戰士
            if (collision.gameObject.tag == "Hurt3")
            {
                invincibleTimeCount = attackedInvincibleTimeSet;
                starkTimeCount = s_AttackedTimeSet;
                LevelSetting.deadTimeCount -= 10;

                if (collision.gameObject.transform.position.x > transform.position.x)
                    rb.velocity = new Vector3(-2.082f, 0, 0);
                if (collision.gameObject.transform.position.x < transform.position.x)
                    rb.velocity = new Vector3(2.082f, 0, 0);
            }
            //唐納德系列
            //抱頭摔
            if (collision.gameObject.tag == "HugHead")
            {
                invincibleTimeCount = attackedInvincibleTimeSet + hugHeadInAnimation;
                starkTimeCount = s_AttackedTimeSet + hugHeadInAnimation;
                LevelSetting.deadTimeCount -= 10;
                PlayerHealth.inAnimationCount = hugHeadInAnimation;
                if(transform.position.x - donaldTarget.position.x <= 0)
                {
                    playerController.transform.position = new Vector3(donaldTarget.position.x - 3f, donaldTarget.position.y - 0.5f, donaldTarget.position.z);
                }
                if (transform.position.x - donaldTarget.position.x > 0)
                {
                    playerController.transform.position = new Vector3(donaldTarget.position.x + 3f, donaldTarget.position.y - 0.5f, donaldTarget.position.z);
                }
            }
            //跳躍重擊
            if (collision.gameObject.tag == "JumpAttack")
            {
                invincibleTimeCount = attackedInvincibleTimeSet;
                starkTimeCount = s_AttackedTimeSet;
                LevelSetting.deadTimeCount -= 7;

                if (collision.gameObject.transform.position.x > transform.position.x)
                    playerController.transform.position = new Vector3(transform.position.x - speed * 2, transform.position.y, transform.position.z);
                if (collision.gameObject.transform.position.x < transform.position.x)
                    playerController.transform.position = new Vector3(transform.position.x + speed * 2, transform.position.y, transform.position.z);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "SceneWall_U")
        {
            cantUp = true;
        }
        if (collision.gameObject.tag == "SceneWall_B")
        {
            cantDown = true;
        }
        if (collision.gameObject.tag == "SceneWall_L")
        {
            cantLeft = true;
        }
        if (collision.gameObject.tag == "SceneWall_R")
        {
            cantRight = true;
        }
        if (collision.gameObject.tag == "Finish")
        {
            b_FinishPoint = true;
            m_InteractiveButtom.SetActive(true);
        }
        if (collision.gameObject.tag == "Back")
        {
            b_BackPoint = true;
            m_InteractiveButtom.SetActive(true);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "SceneWall_U")
        {
            cantUp = false;
        }
        if (collision.gameObject.tag == "SceneWall_B")
        {
            cantDown = false;
        }
        if (collision.gameObject.tag == "SceneWall_L" || collision.gameObject.tag == "Back")
        {
            cantLeft = false;
        }
        if (collision.gameObject.tag == "SceneWall_R" || collision.gameObject.tag == "Finish") 
        {
            cantRight = false;
        }
        if (collision.gameObject.tag == "Finish")
        {
            b_FinishPoint = false;
            m_InteractiveButtom.SetActive(false);
        }
        if (collision.gameObject.tag == "Back")
        {
            b_BackPoint = false;
            m_InteractiveButtom.SetActive(false);
        }
    }
}
