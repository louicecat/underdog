using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BerserkerCtrl : MonoBehaviour
{
    public bool beAlarmed;
    float playerRange_x;
    float playerRange_y;
    float playerRange = 30;
    Transform playerTarget;
    Vector3 truePlayerTarget;

    public float starkTimeCount;

    public float attackStep;

    [Header("速度")]
    public float speed = 0.75f;
    [Header("HP")]
    public int Hp = 3;

    public float attackTimeCount = 0;
    [Header("準備時間")]
    public float attackReadyTime = 0.5f;
    [Header("攻擊時間= 8(0.5=1單位)/speed/8(速度)")]
    public float attackTime = 5.3f;
    [Header("硬直時間")]
    public float attackAfterTime = 2f;

    public GameObject readyAttack;
    public GameObject attack;

    bool attacking;
    // Start is called before the first frame update
    void Start()
    {
        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        truePlayerTarget = new Vector3(playerTarget.position.x, playerTarget.position.y - 0.62f, playerTarget.position.z);
        playerRange_x = Mathf.Abs(truePlayerTarget.x - gameObject.transform.position.x);
        playerRange_y = Mathf.Abs(truePlayerTarget.y - gameObject.transform.position.y);
        playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

        if (beAlarmed == false)
        {
            if (playerRange < 9)
            {
                beAlarmed = true;
            }
        }
        if (beAlarmed == true)
        {
            if (starkTimeCount <= 0)
            {
                Attack();
                if (attackStep == 0)
                {
                    Move();
                }
            }
            else
            {
                starkTimeCount -= Time.deltaTime;
            }
        }
    }

    //void OnDrawGizmos()//輔助線
    //{
    //    Gizmos.DrawWireCube(gameObject.transform.position, new Vector3(2f, 2, 0));
    //    Gizmos.color = Color.red;
    //    //Gizmos.DrawWireSphere(gameObject.transform.position, 3f);
    //}

    void Attack()
    {
        //距離
        playerRange_x = Mathf.Abs(truePlayerTarget.x - gameObject.transform.position.x);
        playerRange_y = Mathf.Abs(truePlayerTarget.y - gameObject.transform.position.y);
        playerRange = Mathf.Sqrt(playerRange_x * playerRange_x + playerRange_y + playerRange_y);

        if (playerRange_x <= 10f && playerRange_y <= 1f && attackStep == 0)
        {
            if (truePlayerTarget.x > transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                attackTimeCount = 0;
                attackStep = 1;
            }
            if (truePlayerTarget.x < transform.position.x)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                attackTimeCount = 0;
                attackStep = 1.1f;
            }
        }

        if (attackStep == 1)
        {
            readyAttack.SetActive(true);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackReadyTime)
            {
                attackTime = 1.666f;//2是1(距)/8(速)=2(時)
                attackTimeCount = 0;
                attackStep = 2;
            }
        }
        if (attackStep == 1.1f)
        {
            readyAttack.SetActive(true);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackReadyTime)
            {
                attackTime = 1.666f;
                attackTimeCount = 0;
                attackStep = 3;

            }
        }
        if (attackStep == 2)
        {
            attack.SetActive(true);
            readyAttack.SetActive(false);
            attackTimeCount += Time.deltaTime;

            transform.position = transform.position + Vector3.right * Time.deltaTime * speed * 12;
            if (attackTimeCount >= attackTime)
            {
                attackTimeCount = 0;
                attackStep = 4;
            }
        }
        if (attackStep == 3)
        {
            attack.SetActive(true);
            readyAttack.SetActive(false);
            attackTimeCount += Time.deltaTime;

            transform.position = transform.position + Vector3.left * Time.deltaTime * speed * 12;
            if(attackTimeCount >= attackTime)
            {
                attackTimeCount = 0;
                attackStep = 4;
            }
        }
        if(attackStep == 4)
        {
            attack.SetActive(false);
            attackTimeCount += Time.deltaTime;
            if (attackTimeCount >= attackAfterTime)
            {
                attackTimeCount = 0;
                attackStep = 0;
            }
        }
    }

    void Move()
    {
        if (playerTarget.transform.position.x > transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (playerTarget.transform.position.x < transform.position.x)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        if (playerTarget.transform.position.x - transform.position.x > 3f || playerTarget.transform.position.x - transform.position.x < -3f)
        {
            transform.position = Vector2.MoveTowards(transform.position, playerTarget.position, speed * Time.deltaTime * 2);
        }
        else if(playerTarget.transform.position.x - transform.position.x < 3f && playerTarget.transform.position.y - transform.position.y > 0)
        {
            transform.position += Vector3.left * speed * Time.deltaTime * 1.414f;
            transform.position += Vector3.up * speed * Time.deltaTime * 1.414f;
        }
        else if (playerTarget.transform.position.x - transform.position.x > -3f && playerTarget.transform.position.y - transform.position.y > 0)
        {
            transform.position += Vector3.right * speed * Time.deltaTime * 1.414f;
            transform.position += Vector3.up * speed * Time.deltaTime * 1.414f;
        }
        else if (playerTarget.transform.position.x - transform.position.x < 3f && playerTarget.transform.position.y - transform.position.y < 0)
        {
            transform.position += Vector3.left * speed * Time.deltaTime * 1.414f;
            transform.position += Vector3.down * speed * Time.deltaTime * 1.414f;
        }
        else if (playerTarget.transform.position.x - transform.position.x > -3f && playerTarget.transform.position.y - transform.position.y < 0)
        {
            transform.position += Vector3.right * speed * Time.deltaTime * 1.414f;
            transform.position += Vector3.down * speed * Time.deltaTime * 1.414f;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Attack")
        {
            Debug.Log("1");
            //補玩家血
            LevelSetting.deadTimeCount += 1;
            //扣血
            Hp -= 1;
            //死亡
            if (Hp <= 0)
            {
                Destroy(gameObject);//到時候改動畫
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && attackStep == 2 || collision.gameObject.tag == "Player" && attackStep == 3)//條件還有障礙物沒加
        {
            Debug.Log("1111");
            attackTimeCount = 0;
            attackStep = 4;
        }
    }
}