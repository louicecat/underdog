using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonaldJumpCtrl : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "JumpAttack")
        {
            Destroy(gameObject);
        }
    }
}
