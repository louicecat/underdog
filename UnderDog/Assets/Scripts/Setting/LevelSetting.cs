using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSetting : MonoBehaviour
{
    //偵測敵人是否清空
    GameObject enemySave;
    bool enemyIsClear = true;
    public GameObject stageClearCG;
    [Header("回家鈕")]
    public GameObject backToHomeBtn;
    [Header("睡覺鈕")]
    public GameObject sleepBtn;

    //過場
    float timeCount;
    [Header("過場時間")]
    public float timeSet = 3;
    public static bool isTransitions = false;
    [Header("過場圖(在Main Camera)")]
    public GameObject img_Transitions;

    //終端
    //public GameObject m_Camera;
    public GameObject m_Player;
    public GameObject phone;
    public GameObject phoneScreen;
    public static bool b_PhoneScreen = false;
    public static bool b_Phone = true;
    public static float nowLevel = 0;

    //時間控制
    public static float deadTimeCount = 0;
    [Header("時間最大值")]
    public float LevelMaxTime = 60;
    [Header("連續戰鬥時間增加")]
    public float LevelAddTime = 5;
    public GameObject m_DeadTime;
    public static bool timeIsSet;
    public Text t_DeadTime;

    // Start is called before the first frame update
    void Start()
    {
        enemyIsClear = true;
        deadTimeCount = LevelMaxTime;
        isTransitions = false;
        timeCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //m_Camera = GameObject.FindGameObjectWithTag("MainCamera");
        /*enemySave = GameObject.FindGameObjectWithTag("Enemy");
        if (enemySave == null)
        {
            Debug.Log("A");
        }
        else
        {
            Debug.Log("B");
        }*/
        //終端控制套用(83~)
        //睡覺
        //回家
        PhoneCtrl();
        //時間控制
        DeadTimeControl();
        //過場
        if (isTransitions == false)
        {
            img_Transitions.SetActive(true);
            timeCount = timeCount + Time.deltaTime;
            if(timeCount > timeSet)
            {
                img_Transitions.SetActive(false);
                timeCount = 0;
                isTransitions = true;
            }
        }
    }

    //終端控制
    void PhoneCtrl()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && nowLevel <= 0) 
        {
            b_Phone = !b_Phone;
            b_PhoneScreen = !b_PhoneScreen;
        }
        if (b_Phone == true)
            phone.SetActive(true);
        else
            phone.SetActive(false);

        if (b_PhoneScreen == true)
            phoneScreen.SetActive(true);
        else
            phoneScreen.SetActive(false);
    }
    public void PhoneOnClick()
    {
        b_PhoneScreen = true;
        b_Phone = false;
    }
    public void PhoneScreenClickOut()
    {
        b_PhoneScreen = false;
        b_Phone = true;
    }

    //選關
    public void LivingRoomChoose()
    {
        sleepBtn.SetActive(true);
        backToHomeBtn.SetActive(false);
        isTransitions = false;
        b_PhoneScreen = false;
        b_Phone = true;
        m_Player.transform.position = new Vector3(-2000, 0, 0);//玩家客廳位置
        nowLevel = 0;
        SceneManager.LoadScene(2);
    }
    public void LevelOneChoose()
    {
        sleepBtn.SetActive(false);
        backToHomeBtn.SetActive(true);
        enemyIsClear = true;
        timeIsSet = false;
        isTransitions = false;
        b_PhoneScreen = false;
        b_Phone = false;
        LevelSetting.nowLevel = 1;
        m_Player.transform.position = new Vector3(-5, -1.5f, 0);
        nowLevel = 1;
        SceneManager.LoadScene(3);
    }

    //時間控制
    void DeadTimeControl()
    {
        if (nowLevel > 0 && isTransitions == true)
        {
            m_DeadTime.SetActive(true);
            if (timeIsSet == false)
            {
                deadTimeCount += LevelAddTime;
                if(deadTimeCount > LevelMaxTime)
                {
                    deadTimeCount = LevelMaxTime;
                }
                timeIsSet = true;
            }
            enemySave = GameObject.FindGameObjectWithTag("Enemy");
            if (enemySave == null && enemyIsClear == false)
            {
                stageClearCG.SetActive(true);
                enemyIsClear = true;
            }
            if(enemySave != null)
            {
                deadTimeCount = deadTimeCount - Time.deltaTime;
                enemyIsClear = false;
            }
        }
        else
        {
            m_DeadTime.SetActive(false);
        }

        if(deadTimeCount < 0)
        {
            sleepBtn.SetActive(true);
            backToHomeBtn.SetActive(false);
            isTransitions = false;
            b_PhoneScreen = false;
            b_Phone = true;
            //m_Camera.transform.position = new Vector3(0, 0, -10);
            nowLevel = 0;
            m_Player.transform.position = new Vector3(-2000, -2, 0);
            nowLevel = 0;
            deadTimeCount = 0;
            SceneManager.LoadScene(2);
            //死亡加天
        }

        t_DeadTime.text = Mathf.Floor(deadTimeCount).ToString();
    }
    public void Sleep()
    {
        deadTimeCount = LevelMaxTime;
        //加天
    }
    public void BackToHome()
    {
        sleepBtn.SetActive(true);
        backToHomeBtn.SetActive(false);
        isTransitions = false;
        b_PhoneScreen = false;
        b_Phone = true;
        //m_Camera.transform.position = new Vector3(0, 0, -10);
        m_Player.transform.position = new Vector3(-2000, 0, 0);
        nowLevel = 0;
        SceneManager.LoadScene(2);
    }
}
