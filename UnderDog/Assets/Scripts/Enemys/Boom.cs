using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boom : MonoBehaviour
{
    [Header("�u�t")]
    public float speed;
    public GameObject boom;
    public int type;
    Transform boomTarget;
    public GameObject boomBullet;
    public GameObject readyBoom;
    public Transform boomEnemy;
    GameObject target_boomEnemy;


    private void Start()
    {
        if (type == 1)
        {
            transform.position = boomEnemy.transform.position;
        }
    }
    void Update()
    {
        if (type == 1)
        {
            boomTarget = GameObject.FindGameObjectWithTag("BoomTarget").GetComponent<Transform>();
            boomBullet.transform.position = Vector2.MoveTowards(transform.position, boomTarget.position, speed * Time.deltaTime);
        }
        if (type == 2) 
        {
            target_boomEnemy = GameObject.FindGameObjectWithTag("BoomEnemy");
            if(target_boomEnemy == null)
            {
                Destroy(gameObject);
            }
            if(target_boomEnemy != null)
            {

            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "BoomTarget"  && type == 1)
        {
            gameObject.transform.position = boomEnemy.transform.position;
            boomBullet.SetActive(false);
        }
        if (collision.tag == "BoomBullet" && type == 2)
        {
            readyBoom.SetActive(false);
            boom.SetActive(true);
            Destroy(gameObject, 0.3f);
        }
    }
}
